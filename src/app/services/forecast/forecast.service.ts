import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, URLSearchParams } from '@angular/http';
import { Observable, Subject } from 'rxjs';
import { IWeatherRequest } from './forecast.model';

@Injectable()

export class ForecastService {

    private static BASE_URL: string = 'http://api.openweathermap.org/data/2.5';
    private static API_KEY: string = '6133300578113e08d5798b9dbbbdc890';

    public errorStatus$: Observable<boolean>;

    private errorStatus: Subject<any>;

    constructor(private http: Http) {
        this.errorStatus = new Subject();
        this.errorStatus$ = this.errorStatus.asObservable();
    }

    public getForecast(params: IWeatherRequest): Observable<any> {
        this.errorStatus.next(false);
        let requestOptions = this.getUrlParams(params);

        return this.http.get(ForecastService.BASE_URL + '/forecast/daily', requestOptions)
            .map((res: Response) => res.json())
            .catch(this.handleError.bind(this));
    }

    public getWeather(params: IWeatherRequest): Observable<any> {
        this.errorStatus.next(false);
        let requestOptions = this.getUrlParams(params);

        return this.http.get(ForecastService.BASE_URL + '/weather', requestOptions)
            .map((res: Response) => res.json())
            .catch(this.handleError.bind(this));
    }

    private handleError(error: any) {
        this.errorStatus.next(error.json());
        console.error('smtn went wrong', error);
        return Observable.throw(error.message || error);
    }

    private getUrlParams(params: IWeatherRequest): RequestOptions {
        let requestOptions = new RequestOptions();
        let urlParams: URLSearchParams = new URLSearchParams();

        urlParams.set('appid', ForecastService.API_KEY);
        urlParams.set('q', params.city);
        urlParams.set('units', params.units);
        urlParams.set('lang', params.lang);

        if (params.count !== undefined) {
            urlParams.set('cnt', params.count.toString());
        }

        requestOptions.params = urlParams;
        return requestOptions;
    }
}
