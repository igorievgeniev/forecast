export interface IWeatherRequest {
    city: string;
    count?: number;
    units: string;
    lang: string;
}

export interface IWeatherResponse {
    coord: ICoord;
    weather: IWeather[];
    base: string;
    main: IMain;
    wind: IWind;
    clouds: IClouds;
    visibility: number;
    dt: number;
    sys: ISys;
    id: number;
    name: string;
    cod: number;
}

export interface IForecastResponse {
    city: ICity;
    cnt: number;
    cod: string;
    list: IForecastListItem[];
    message: number;
}

export interface ICoord {
    lon: number;
    lat: number;
}

export interface IWeather {
    id: number;
    main: string;
    description: string;
    icon: string;
}

export interface IMain {
    temp: number;
    pressure: number;
    humidity: number;
    temp_min: number;
    temp_max: number;
}

export interface IWind {
    speed: number;
    deg: number;
}

export interface IClouds {
    all: number;
}

export interface ISys {
    type: number;
    id: number;
    message: number;
    country: string;
    sunrise: number;
    sunset: number;
}

export interface IForecastListItem {
    clouds: number;
    deg: number;
    dt: number;
    humidity: number;
    pressure: number;
    speed: number;
    temp: ITemp;
    weather: IWeather[];
}

export interface ITemp {
    day: number;
    eve: number;
    max: number;
    min: number;
    morn: number;
    night: number;
}

export interface ICity {
    coord: ICoord;
    country: string;
    id: number;
    name: string;
    population: number;
}
