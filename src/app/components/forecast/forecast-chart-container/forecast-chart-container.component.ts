import { Component, Input, OnInit } from '@angular/core';
import { map, find } from 'lodash';
import { DatePipe } from '@angular/common';

export interface IFormatterData {
    type: string;
    data: [{
        data: any[];
        label: string;
    }];
}

@Component({
    selector: 'forecast-chart-container',
    styleUrls: ['./forecast-chart-container.component.scss'],
    templateUrl: './forecast-chart-container.component.html',
    providers: [
        DatePipe
    ]
})

export class ForecastChartContainerComponent implements OnInit {
    @Input() public forecastData: any;

    // tabs
    public activeTab;
    public tabs = [
        {
            id: 'speed',
            title: 'Wind',
            active: false
        },
        {
            id: 'temp',
            title: 'Temperature',
            active: true
        },
        {
            id: 'pressure',
            title: 'Pressure',
            active: false
        },
        {
            id: 'humidity',
            title: 'Humidity',
            active: false
        }
    ];
    // chart
    public chartOptions: any = {
        responsive: true
    };
    public chartType: string = 'bar';
    public chartLegend: boolean = true;
    public chartLabels: string[];
    public chartData: any[] = [{data: [], label: ''}];

    private chartFormatDataStrategy = {
        speed: {
            type: 'line',
            label: 'Wind speed',
            formatData: ((data) => map(data, 'speed'))
        },
        temp: {
            type: 'bar',
            label: 'Max temperature',
            formatData: ((data) => map(map(data, 'temp'), 'max'))
        },
        pressure: {
            type: 'radar',
            label: 'Pressure',
            formatData: ((data) => map(data, 'pressure'))
        },
        humidity: {
            type: 'polarArea',
            label: 'Humidity',
            formatData: ((data) => map(data, 'humidity'))
        }
    };

    constructor(private datePipe: DatePipe) {
    }

    public setActiveTab(tab): void {
        this.tabs.map((t) => t.active = false);
        tab.active = true;
        this.activeTab = tab;
        this.setChartData();
    }

    public ngOnInit(): void {
        if (this.tabs.length) {
            this.setActiveTab(find(this.tabs, {active: true}) || this.tabs[0]);
        }
    }

    private getFormattedData(id: string): IFormatterData {
        const formatStrategy = this.chartFormatDataStrategy[id];

        return {
            type: formatStrategy.type,
            data: [{
                data: formatStrategy.formatData(this.forecastData.list),
                label: formatStrategy.label
            }]
        };
    }

    private getFormattedLabels(): string[] {
        return map(this.forecastData.list, 'dt').map((i: number) => {
            return this.datePipe.transform(i * 1000, 'd MMM');
        });
    }

    private setChartData(): void {
        const charFormattedData = this.getFormattedData(this.activeTab.id);

        this.chartLabels = this.getFormattedLabels();
        this.chartData = charFormattedData.data;
        setTimeout(() => this.chartType = charFormattedData.type, 0);
    }
}
