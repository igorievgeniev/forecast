import { Component, OnInit } from '@angular/core';
import { ForecastService } from '../../services/forecast';
import { IWeatherRequest } from '../../services/forecast/forecast.model';
import { Observable } from 'rxjs';

@Component({
    selector: 'forecast-container',
    providers: [],
    styleUrls: ['./forecast-container.component.scss'],
    templateUrl: './forecast-container.component.html'
})

export class ForecastContainerComponent implements OnInit {

    public forecastData$: Observable<any>;
    public weatherData$: Observable<any>;
    public errorStatus$: Observable<any>;
    private defaultCity = 'London';

    constructor(private forecastService: ForecastService) {
    }

    public fetchData(value: string) {
        this.fetchWeather(value);
        this.fetchForecast(value);
    }

    public ngOnInit(): void {
        this.fetchData(this.defaultCity);
        this.errorStatus$ = this.forecastService.errorStatus$;
    }

    private fetchWeather(value): void {
        let weatherParams: IWeatherRequest = {
            city: value,
            units: 'metric',
            lang: 'en'
        };

        this.weatherData$ = this.forecastService.getWeather(weatherParams);
    }

    private fetchForecast(value: string): void {
        let forecastParams: IWeatherRequest = {
            city: value,
            count: 16,
            units: 'metric',
            lang: 'en'
        };

        this.forecastData$ = this.forecastService.getForecast(forecastParams);
    }
}
