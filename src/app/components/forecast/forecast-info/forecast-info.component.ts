import { Component, Input } from '@angular/core';

@Component({
    selector: 'forecast-info',
    styleUrls: ['./forecast-info.component.scss'],
    templateUrl: './forecast-info.component.html'
})

export class ForecastInfoComponent {
    @Input() public weatherData: any;
}
