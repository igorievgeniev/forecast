import { Component, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'forecast-search',
    providers: [],
    styleUrls: ['./forecast-search.component.scss'],
    templateUrl: './forecast-search.component.html'
})

export class ForecastSearchComponent {
    @Output() public search = new EventEmitter<string>();

    public onSearch(value: string) {
        this.search.emit(value);
    }

}
