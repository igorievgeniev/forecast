import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts';

import { PipesModule } from '../../pipes';
import { ForecastService } from '../../services/forecast/forecast.service';
import {
    ForecastContainerComponent,
    ForecastSearchComponent,
    ForecastInfoComponent,
    ForecastChartContainerComponent
} from './';

@NgModule({
    declarations: [
        ForecastContainerComponent,
        ForecastSearchComponent,
        ForecastInfoComponent,
        ForecastChartContainerComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ChartsModule,
        PipesModule,
    ],
    exports: [
        ForecastContainerComponent,
        ChartsModule
    ],
    providers: [
        ForecastService
    ]
})

export class ForecastModule {

}
