import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ROUTES } from './app.routes';
import { AppComponent } from './app.component';
import { ForecastModule } from './components/forecast/forecast.module';

import '../styles/styles.scss';

@NgModule({
    bootstrap: [AppComponent],
    declarations: [
        AppComponent
    ],

    imports: [
        ForecastModule,
        HttpModule,
        RouterModule.forRoot(ROUTES)
    ]
})
export class AppModule {

}
