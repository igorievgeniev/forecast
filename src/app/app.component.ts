import {
    Component,
    ViewEncapsulation
} from '@angular/core';

import { ForecastContainerComponent } from './components/forecast/forecast-container.component';

@Component({
    selector: 'app',
    // encapsulation: ViewEncapsulation.None,
    template: `
        <forecast-container></forecast-container>`
})
export class AppComponent {
}
