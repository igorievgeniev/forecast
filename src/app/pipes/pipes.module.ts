import { NgModule } from '@angular/core';

import { CapitalizeFirstLetterPipe } from './capitalizeFirstLetter/capitalizefirstletter.pipe';

@NgModule({
    declarations: [
        CapitalizeFirstLetterPipe
    ],
    imports: [],
    exports: [
        CapitalizeFirstLetterPipe
    ]
})
export class PipesModule {

}
