import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'capitalizeFirstLetter'
})

export class CapitalizeFirstLetterPipe implements PipeTransform {
    public transform(value: string, args: any[]): string {
        return value === null ? 'Not assigned' : value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
    }
}
